
        //rango de dados
        int rangoBajo = 1;
        int rangoAlto = 6;
        
        int dado1 = 0;
        int dado2 = 0;
        int jugadorGanador = 0;
        int jugadorTurno = 1;
        Random randomDados = new Random();
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Presione enter para cambiar de jugador");
            scanner.nextLine();
            System.out.println("Turno para jugador : "+jugadorTurno);
            dado1 = randomDados.nextInt(rangoAlto - rangoBajo) + rangoBajo;
            dado2 = randomDados.nextInt(rangoAlto - rangoBajo) + rangoBajo;
            System.out.println("El jugador: "+jugadorTurno + " ha obtenido estos dados: ");
            System.out.println("Dado 1: " + dado1);
            System.out.println("Dado 2: " + dado2);

            if(dado1 == dado2 && dado2 == dado1){
                jugadorGanador = jugadorTurno;
            }

            jugadorTurno++;

        } while (jugadorTurno <= 4);

        System.out.println( jugadorGanador != 0 ?
            "El ganador es : " + jugadorGanador
            :
            "Nadie gano jaja :("
        );
