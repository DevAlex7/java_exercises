package com.example.parcial;

import java.util.Random;
import java.util.Scanner;

public class Parcial {
    public static void main(String[] args) {
        double totalPuntos = 0;
        int opcion = 0,
        factorAEvaluar=0,
        rangoBajo = 0,
        rangoAlto = 0,
        acumuladorPreguntas = 1 ,
        totalPreguntas = 5,
        puntos = 2,
        respuesta,
        dificultad = 0,
        segundoFactor=0,
        seleccionOperador = 0 ; //0 es multiplicacion y 1 es suma

        Random randomFactor = new Random();

        Scanner entrada = new Scanner(System.in);

        do {
            System.out.println("Menu de selección: 1-Configuración 2-Ejecución del test 3-Salir del sistema");
            opcion = entrada.nextInt();

            if(opcion == 1){
                System.out.println("Configuracion");

                System.out.println("Que operador desea para su examen? 0 - multiplicacion 1 - suma");
                seleccionOperador = entrada.nextInt();

                switch (seleccionOperador){
                    case 0:
                        //multiplicacion

                        //el usuario ingresara el factor a evaluar
                        System.out.println("Escoja un factor entre 2 y 10");
                        factorAEvaluar = entrada.nextInt();
                        System.out.println("Factor a evaluar es: " + factorAEvaluar);

                        //el usuario seleccionara la dificultad
                        System.out.println("Seleccione un nivel de dificultad: 1-facil 2-normal 3-avanzado");
                        dificultad = entrada.nextInt();
                        System.out.println(dificultad);

                        System.out.println("Se ha seleccionado "+dificultad);
                        break;

                    case 1:
                        //suma

                        //el usuario ingresara el factor a evaluar
                        System.out.println("Escoja un factor entre 2 y 10");
                        factorAEvaluar = entrada.nextInt();
                        System.out.println("Factor a evaluar es: " + factorAEvaluar);

                        //el usuario seleccionara la dificultad
                        System.out.println("Seleccione un nivel de dificultad: 1-facil 2-normal 3-avanzado");
                        dificultad = entrada.nextInt();
                        System.out.println(dificultad);

                        System.out.println("Se ha seleccionado "+dificultad);
                        break;
                    default:
                    System.out.println("seleccion no disponible");
                    break;
                }
            }
            else if(opcion == 2){

                System.out.println("Dificultad seleccionada:" + dificultad);

                for (int x = 1; x<=5; x++){

                    rangoAlto = dificultad == 1 ? 6 : dificultad == 2 ? 10 : 10;
                    rangoBajo = dificultad == 1 ? 1 : dificultad == 2 ? 1 : 4;

                    segundoFactor = randomFactor.nextInt(rangoAlto - rangoBajo) + rangoBajo;

                    System.out.println(seleccionOperador == 0 ? "Cuanto es: "+factorAEvaluar+ " x "+segundoFactor : "Cuanto es: "+factorAEvaluar+ " + "+segundoFactor);

                    int resultado = seleccionOperador == 0 ? factorAEvaluar * segundoFactor : factorAEvaluar + segundoFactor;

                    respuesta = entrada.nextInt();

                    if(resultado == respuesta){
                        System.out.println("correcto");
                        totalPuntos = totalPuntos + 2;
                    }
                    else{
                        System.out.println("incorrecto");
                        totalPuntos = totalPuntos - 0.5;
                    }
                }

                System.out.println(totalPuntos > 5 ?
                    "Resultado: "+totalPuntos + ". Has aprobado."
                    :
                    "Resultado: "+totalPuntos + ". No has aprobado."
                );

            }
            else{
                return;
            }


        } while (true);
    }
}

